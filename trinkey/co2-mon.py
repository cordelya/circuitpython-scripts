# CO2 Monitor for TrinKey
# This script will poll an Adafruit SCD30 STEMMA CO2 Sensor
# at the designated interval and display the information
# on a connected I2C screen.

import time
import board
import busio
import displayio
import terminalio
import neopixel
import adafruit_scd30
import adafruit_displayio_ssd1306
from adafruit_display_text import label

# Set Variables

REFRESH = 5 # how often should the readings be updated? (seconds)

# What color values should we use?
green = 3, 252, 44 # the color to use for "good" levels (from 0x03fc2c)
yellow =  240, 252, 3 # the color to use for "almost bad" levels (from 0xf0fc03)
red =  252, 3, 3 # the color to use for "bad" levels (from 0xfc0303)

# What CO2 thresholds do we want?
CO2_HIGH = 1200
CO2_LOW = 800

# Tell the script what your screen's dimensions are
# Note: script was written for a very small display
# Using a larger screen may give weird or unpredictable results
WIDTH = 128
HEIGHT = 32  # Change to 64 if needed
BORDER = 5

# initialize our peripherals
displayio.release_displays()
i2c = busio.I2C(board.SCL, board.SDA, frequency=50000)
led = neopixel.NeoPixel(board.NEOPIXEL, 1)
sensor = adafruit_scd30.SCD30(i2c)
display_bus = displayio.I2CDisplay(i2c, device_address=0x3C)
display = adafruit_displayio_ssd1306.SSD1306(display_bus, width=WIDTH, height=HEIGHT)
splash = displayio.Group()
display.show(splash)

#display initial message
bg = displayio.Bitmap(WIDTH, HEIGHT, 1)
inner_palette = displayio.Palette(1)
inner_palette[0] = 0x000000  # Black
inner_sprite = displayio.TileGrid(
    bg, pixel_shader=inner_palette, x=0, y=0
)
splash.append(inner_sprite)

# Draw a label
text = "CO2 Monitor"
text_area = label.Label(
    terminalio.FONT, text=text, color=0xFFFFFF, x=28, y=HEIGHT // 2 - 1
)
splash.append(text_area)
line1 = ' ' * 9
line2 = ' ' * 15

# Create the Data Display Lines
text_line1 = label.Label(
    terminalio.FONT,
    text=line1,
    color=0xFFFFFF
)
text_line1.anchor_point = 0.5, 0.0,
text_line1.anchored_position = (WIDTH / 2, 0)

text_line2 = label.Label(
    terminalio.FONT,
    text=line2,
    color=0xFFFFFF
)
text_line2.anchor_point = 0.5, 0.0,
text_line2.anchored_position = (WIDTH / 2, int(HEIGHT * 0.55))

splash.append(text_line1)
splash.append(text_line2)
display.show(splash)
#pause to give the CO2 sensor time to start up
time.sleep(2)
text_area.text = ''
while True:
    while sensor.data_available:
        temperature = round((sensor.temperature * 9/5) + 32, 1) # get temp, convert to F
        co2 = int(sensor.CO2)
        humidity = round(sensor.relative_humidity, 1)

    line1 = str(co2) + " PPM"
    line2 = str(temperature) + "F | " + str(humidity) + "% rH"
    print(line1 + " | " + line2)

    text_line1.text = line1
    text_line2.text = line2

    # update the neopixel
    if co2 > CO2_HIGH:
        led.fill(red)
    elif co2 > CO2_LOW:
        led.fill(yellow)
    else:
        led.fill(green)
    time.sleep(REFRESH)
