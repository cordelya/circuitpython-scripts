# Plant Monitor for TrinKey
# Hardware Requirements: TrinKey, Seeed I2C Soil Sensor, display module
# Package Requirements: see import list
# When the Seeed I2C Soil Sensor is inserted into potting soil in a plant
# pot, the capacitive moisture reading and temperature will be displayed
# on the screen.
# It is up to you to determine what the numeric readings mean, and what
# thresholds to use for "needs watering" vs "doesn't need watering"
# as it will vary for each kind of plant.

import time
import board
import displayio
import terminalio
from adafruit_display_text import label
import adafruit_displayio_ssd1306
from adafruit_seesaw.seesaw import Seesaw

displayio.release_displays()

i2c = board.I2C()

ss = Seesaw(i2c, addr=0x36)
display_bus = displayio.I2CDisplay(i2c, device_address=0x3C)

# Tell the script what your screen's dimensions are
# Note: script was written for a very small display
# Using a larger screen may give weird or unpredictable results
WIDTH = 128
HEIGHT = 32  # Change to 64 if needed
BORDER = 5

# how often should the readings be updated? (seconds)
REFRESH = 5

display = adafruit_displayio_ssd1306.SSD1306(display_bus, width=WIDTH, height=HEIGHT)

splash = displayio.Group()
display.show(splash)

#display initial message
bg = displayio.Bitmap(WIDTH, HEIGHT, 1)
inner_palette = displayio.Palette(1)
inner_palette[0] = 0x000000  # Black
inner_sprite = displayio.TileGrid(
    bg, pixel_shader=inner_palette, x=0, y=0
)
splash.append(inner_sprite)

# Draw a label
text = "Plant Monitor"
text_area = label.Label(
    terminalio.FONT, text=text, color=0xFFFFFF, x=28, y=HEIGHT // 2 - 1
)
splash.append(text_area)

while True:
    # read moisture level through capacitive touch pad
    touch = ss.moisture_read()

    # read temperature from the temperature sensor
    temp = ss.get_temp()
    temp = (temp * 9 / 5) + 32

    print("temp: " + str(temp) + "  moisture: " + str(touch))
    print((touch, temp))

    readings = displayio.Group()

    display.show(readings)

    #display initial message
    bg = displayio.Bitmap(WIDTH, HEIGHT, 1)
    inner_palette = displayio.Palette(1)
    inner_palette[0] = 0x000000  # Black
    inner_sprite = displayio.TileGrid(
        bg, pixel_shader=inner_palette, x=0, y=0
    )
    readings.append(inner_sprite)

    # Draw a label
    text = str(round(temp,1)) + "F " + str(touch) + "M"
    text_area = label.Label(
        terminalio.FONT, text=text, color=0xFFFFFF, x=28, y=HEIGHT // 2 - 1
    )
    readings.append(text_area)
    time.sleep(REFRESH)
