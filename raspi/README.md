# Raspberry Pi Scripts #

This directory contains a collection of CircuitPython scripts meant to run on a Raspberry Pi. See top of each script for notes on required Python packages or hardware.
