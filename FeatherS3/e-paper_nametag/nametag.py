# 2.9" Grayscale EPD Badge with label switcher
# adapted from the "Simple Test Script" for the 2.9" grayscale display
# SPDX-License-Identifier: MIT

"""EPD Badge with Label Switcher

Supported products:
  * Adafruit 2.9" Grayscale
    * https://www.adafruit.com/product/4777
  * Feather S3
  """

import os
import time
import busio
import board
import displayio, terminalio, digitalio
from adafruit_display_text.label import Label
from adafruit_display_shapes.rect import Rect
from adafruit_display_shapes.roundrect import RoundRect
from adafruit_bitmap_font import bitmap_font
import adafruit_il0373
import adafruit_sdcard
import storage
import json

# Functions

def profile_update(button):
    # expects the following format for 'button':
    # button = {"button": "a0"} where 'a0' is one of
    # a|b|c in the first char and 0|1 in the second.
    jsontext = json.dumps(button)
    try:
        with open("params.json", "w") as f:
            f.write(jsontext)
    except:
        if DEBUG:
            print("Could not write current params to the board.")
        # writing to the board didn't work. Try the SD as backup?
        try:
            with open("/sd/params.json", "w") as f:
                f.write(jsontext)
            if DEBUG:
                print("Successfully wrote params to the SD card")
        except:
            if DEBUG:
                print("Could not write params to the SD Card")
            else:
                pass

def screen_refresh(refreshed):
    # if a sufficient amount of time hasn't passed since last refresh, wait
    if (display.time_to_refresh) > 0:
        #we only need to wait the remaining time
        if DEBUG:
            print("Waiting {} seconds before refreshing again.".format(display.time_to_refresh + 1))
        time.sleep(display.time_to_refresh + 1)
    # *now* we can refresh the screen
    display.refresh()
    if DEBUG:
        print("Display refreshed!")

def update_labels(button):
    for btn in range(0, 6, 1):
        splash[views_index][btn].hidden = True
    splash[views_index][button].hidden = False

    display.show(splash)

def init_labels():
    if DEBUG:
        print("Initializing button views..")
    for button in range(0, 6, 1):
        button = str(button)
        group = displayio.Group()
        if DEBUG:
            print("Button {}".format(button))
        # fetch the labels for this button
        line1 = labels[button]["name"]
        line2 = labels[button]["title"]
        line3 = labels[button]["agency"]
        line4 = labels[button]["extra"]
        # create the Label objects for this button
        label1 = Label(font_verdana18, text=line1, color=palette[0])
        label2 = Label(font_verdana10, text=line2, color=palette[0])
        label3 = Label(font_verdana14, text=line3, color=palette[0])
        label4 = Label(font_verdana8, text=line4, color=palette[0])

        # calculate and set position of each label
        label1.x, label1.y = int(display_width/2 - label1.width/2), 20
        label2.x, label2.y = int(display_width/2 - label2.width/2), 45
        label3.x = int(display_width/2 - label3.width/2)
        label3.y = int((display_height - 10) - int(label3.height + label4.height))
        label4.x = int((display_width/2) - (label4.width/2))
        label4.y = int((display_height - 10) - label4.height)
        group.append(label1)
        group.append(label2)
        group.append(label3)
        group.append(label4)

        views.append(group)

time_refreshed = time.monotonic()

# debug?
DEBUG = False

# display dimensions
display_width = 296
display_height = 128

# Colors for 4-color Grayscale E-Paper Display
c_black = 0x000000
c_dk_gray = 0x70696b
c_lt_gray = 0xb1afad
c_white = 0xffffff

# Init the physical buttons on the FeatherWing EPD
# Note: these will only be in "A, B, C" order when
# the board is oriented with the buttons across the top
# (which means a screen rotation of 270)

# Button A will toggle views 0 and 1
# Button B will toggle views 2 and 3
# Button C will toggle views 4 and 5
BUTTON_A = digitalio.DigitalInOut(board.D11)
BUTTON_B = digitalio.DigitalInOut(board.D12)
BUTTON_C = digitalio.DigitalInOut(board.D13)

BUTTON_A.direction = digitalio.Direction.INPUT
BUTTON_A.pull = digitalio.Pull.UP
BUTTON_B.direction = digitalio.Direction.INPUT
BUTTON_B.pull = digitalio.Pull.UP
BUTTON_C.direction = digitalio.Direction.INPUT
BUTTON_C.pull = digitalio.Pull.UP

# Button Default Values
# Make sure you're not holding down any of the 3 buttons
# when booting up the board, or these will not work right
a_default = BUTTON_A.value
b_default = BUTTON_B.value
c_default = BUTTON_C.value

# Button Toggle Default Values
a_toggle = False
b_toggle = False
c_toggle = False

displayio.release_displays()

# This pinout works on a Feather S3 and may need to be altered for other boards.
spi = busio.SPI(board.SCK, board.MOSI, board.MISO)  # Uses SCK and MOSI
epd_cs = board.D9
epd_dc = board.D10
srcs = board.D6
#rst = digitalio.DigitalInOut()
#busy = digitalio.DigitalInOut()
SD_CS = board.D5
sd_avail = False

# mount the SD card if one is inserted
try:
    sdcs = digitalio.DigitalInOut(SD_CS)
    sdcard = adafruit_sdcard.SDCard(spi, sdcs)
    vfs = storage.VfsFat(sdcard)
    storage.mount(vfs, "/sd")
    sd_avail = True
except:
    pass


# Load some nice fonts
font_verdana18 = bitmap_font.load_font("/fonts/verdana18.bdf")
font_verdana14 = bitmap_font.load_font("/fonts/verdana14.bdf")
font_verdana10 = bitmap_font.load_font("/fonts/verdana10.bdf")
font_verdana8 = bitmap_font.load_font("/fonts/verdana8.bdf")
# preload common glyphs
font_verdana18.load_glyphs(b'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890- ()')
font_verdana14.load_glyphs(b'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890- ()')
font_verdana10.load_glyphs(b'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890- ()')
font_verdana8.load_glyphs(b'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890- ()')
# Try to open labels.json file on the SD card & save to board storage.
# This is to allow editing labels without needing board USB access.
# With board powered off, eject SD card, edit sample-labels.json and
# save as labels.json. When done, re-insert the SD card into the slot
# and power the board back up.
try:
    with open("/sd/labels.json", "r") as f:
        with open("labels.json", "w") as g:
            g.write(f)
    os.remove("/sd/labels.json")
except:
    if DEBUG:
        print("Updated Labels File not Found. Continuing.")
    else:
        pass

display_bus = displayio.FourWire(
    spi, command=epd_dc, chip_select=epd_cs, baudrate=1000000
)

display = adafruit_il0373.IL0373(
    bus=display_bus,
    height=128,
    width=296,
    rotation=90,
    black_bits_inverted=False,
    color_bits_inverted=False,
    grayscale=True,
    refresh_time=1,

)

time.sleep(1)

palette = displayio.Palette(4)
palette[0] = 0x000000
palette[1] = 0x70696b
palette[2] = 0xb1afad
palette[3] = 0xffffff

# main display Group
splash = displayio.Group()

# create background and add to splash group
bitmap = displayio.Bitmap(display_width, display_height, 4)
tile_grid = displayio.TileGrid(bitmap, pixel_shader=palette)
splash.append(tile_grid)

# create the border and add to the splash group
outer_rect = Rect(0, 0, display_width, display_height, fill=c_white, outline=c_lt_gray, stroke=10)
splash.append(outer_rect)

# insert the gradient horizontal line
horiz_line = displayio.Group()
image_file = open("/horizontal-line.bmp", "rb")
image = displayio.OnDiskBitmap(image_file)
image_sprite = displayio.TileGrid(image, pixel_shader=getattr(image, 'pixel_shader', displayio.ColorConverter()))
horiz_line.append(image_sprite)
horiz_line.x = int(display_width/2 - 120)
horiz_line.y = int(display_height/2)
splash.append(horiz_line)
display.show(splash)

# load labels
try:
    with open("labels.json", "r") as infile:
        params = json.load(infile)

    labels = params[0]["labels"]
    layouts = params[0]["layouts"]
except:
    if DEBUG:
        print("could not load the labels")
        time.sleep(30)
        sys.exit()
    else:
        sys.exit()

# Initial button values so we always have a default to fall back on
button_name = "a"
button_toggle = 0
button_locator = int(0)

# load the button's last saved state, if it exists
# if loading from the board fails, see if there's one on the
# SD card.
filename = "params.json"
try:
    with open(filename, "r") as infile:
        jsonfile = json.load(infile)
        button_locator = jsonfile["button"]
        # remove the file
        os.remove(filename)
except:
    print("Could not load params from the board")
    if sd_avail:
        try:
            with open("/sd/params.json", "r") as infile:
                jsonfile = json.load(infile)
                button_locator = jsonfile["button"]
            # remove the file on the SD card
            os.remove("/sd/params.json")
        except:
            print("Could not load params from the SD card")
    else:
        print("SD card not available")

if DEBUG:
    print("Our button's location is {}".format(button_locator))

views = displayio.Group()
views = displayio.Group()
init_labels()
splash.append(views)
views_index = splash.index(views)
if DEBUG:
    print("views group is at index {} in splash group".format(views_index))

update_labels(button_locator)

screen_refresh(time_refreshed)

while True:
    toggle = False
    needs_update = False
    a_state = BUTTON_A.value
    b_state = BUTTON_B.value
    c_state = BUTTON_C.value
    if a_state != a_default:
        button_name = int(0)
        a_toggle = not a_toggle
        toggle = a_toggle
        needs_update = True
        if DEBUG:
            print("Button A pressed, toggled to {}".format(toggle))
    if b_state != b_default:
        button_name = int(2)
        b_toggle = not b_toggle
        toggle = b_toggle
        needs_update = True
        if DEBUG:
            print("Button B pressed, toggled to {}".format(toggle))
    if c_state != c_default:
        button_name = int(4)
        c_toggle = not c_toggle
        toggle = c_toggle
        needs_update = True
        if DEBUG:
            print("Button C pressed, toggled to {}".format(toggle))
    # Only update the screen if a button-press was detected
    if needs_update:
        button_locator = button_name + int(toggle)
        if button_locator > 5:
            button_locator = 0
        if DEBUG:
            print("New button is {}".format(button_locator))
        # update the labels for the screen
        update_labels(button_locator)
        # update params.json
        btn = {"button": button_locator}
        profile_update(btn)
        screen_refresh(time_refreshed)
