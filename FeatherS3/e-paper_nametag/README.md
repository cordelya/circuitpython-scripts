# E-Paper Nametag

## Intro
A slim e-paper nametag that needs power to update but not to display. With support for supplying new display texts via removable SD card.

## Hardware

* [Feather S3](https://unexpectedmaker.com/shop/feathers3) by Unexpected Maker
* [Adafruit 2.9" Grayscale E-Paper Display FeatherWing](https://www.adafruit.com/product/4777)
* USB-C cable for programming and power 

## Process

1. Plug the FeatherWing into the Feather S3 (you may need to solder headers to the S3 first).
2. Connect the Feather S3 to your computer using a USB cable meant for data transfer (a power-only cable will not work)
3. Rename `nametag.py` to `code.py` and `labels-example.json` to `labels.json`
4. Edit the `labels.json` file with your desired nametag information. You can set up to six profiles.
5. Generate .bdf bitmap font files for point sizes 8, 10, 14, and 18. I've used Verdana so my files are named verdana8.bdf, etc. These 4 files should be placed in CIRCUITPY/files/. The script is expecting files named verdana8.bdf, verdana10.bdf, verdana14.bdf, and verdana18.bdf. You can either name your generated font files exactly the same or you can edit the script to refer to your .bdf files by the names you chose for them.
   1. The only method of generating .bdf files that has worked consistently for me is the command-line utility `otf2bdf`. See [Adafruit's otf2bdf guide](https://learn.adafruit.com/custom-fonts-for-pyportal-circuitpython-display/use-otf2bdf) for a primer.
6. You'll need to grab the following libraries from the CircuitPython bundle and drop them into CIRCUITPY/lib/:
   1. adafruit_bitmap_font
   2. adafruit_bus_device
   3. adafruit_display_shapes
   4. adafruit_display_text
   5. adafruit_sdcard
   6. adafruit_ilo373
7. Drop `code.py`, `labels.json`, and `horizontal_line.bmp` into the Feather S3's USB directory ("CIRCUITPY"). If everything is right, the e-paper display should display the "0" profile for button "A".
8. You can select your nametag display by pressing the three buttons that run across the long edge of the e-paper FeatherWing. Each button controls two profiles, and each button-press will toggle back and fourth between the two profiles assigned to that button.

Note that the display can be damaged by having updates made more frequently than every 3 minutes, and that is enshrined/enforced in the display library, so if it hasn't been at least 3 minutes since the display updated (not including across power disconnections), there will be a delay before the display updates while the library waits for those 3 minutes to elapse. Plan extra time for changing the display before you need it.

## Updating the nametag data via SD card

If you drop a copy of labels.json onto an SD card and power up the board, the script will attempt to pull that file and update the copy stored in CIRCUITPY/. On successful update, it will then delete the labels.json file from the SD card. It is recommended that you drop a copy of `labels-example.py` to your SD card and use that as your template - edit the file, then "Save As" `labels.json`.

## Preserving Profile Choice Across Restarts
The script will also attempt to write a file named `params.json` to CIRCUITPY/ that will attempt to preserve your last setting across restarts. If `params.json` is found on the next restart, the board will use the data stored there to select the current profile. This is useful if your preferred or most frequent profile is not in position A0. If you have an SD card inserted, the script will attempt to use the SD card as a fallback datastore (writing to CIRCUITPY/ is disabled when the board is connected as a USB storage device to your computer). If there's no SD card, it'll fail gracefully.

## Enclosure

I haven't done this part yet(!), but once I do, I'll include the STLs for it.
