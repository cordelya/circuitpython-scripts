# Feather S3 Scripts

The scripts in this directory are meant to be run on a Feather S3 (by Unexpected Maker). They may also work on other *Feather* boards, but no guarantees are made.

An additional script for the Feather S3 may be found in the [CO2 Sensor Display](https://gitlab.com/cordelya/co2-sensor-display) repo.
