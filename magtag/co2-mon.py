# CO2 Sensor Script for Adafruit MagTag & Adafruit SCD30 STEMMA CO2 Sensor
# This script will automatically poll the SCD30 sensor at designated interval
# Under normal conditions, it is recommended that you set MAGTAG_REFRESH to 180
# or greater - refreshing e-ink more frequently than 3 minute intervals can
# damage the screen.
# Press the left-hand button (Button A) to toggle the lights.
# Press the right-hand button (Button D) to do a manual poll (needed to turn
# the lights ON, but not OFF).
# Displays current battery voltage.

import time
import board
import neopixel
import busio
import adafruit_scd30
import terminalio
import displayio
from adafruit_magtag.magtag import MagTag
from adafruit_display_text.label import Label
from adafruit_bitmap_font import bitmap_font

# Variables
debug = False
lights = True
MAGTAG_REFRESH = 180 # how long in seconds the script will wait between sensor polls
green = 3, 252, 44 # the color to use for "good" levels (from 0x03fc2c)
yellow =  240, 252, 3 # the color to use for "almost bad" levels (from 0xf0fc03)
red =  252, 3, 3 # the color to use for "bad" levels (from 0xfc0303)
big_font = '/fonts/Nunito-Light-75.bdf'
little_font = '/fonts/Nunito-Black-17.bdf'
DISPLAY_HEIGHT = 128
DISPLAY_WIDTH = 296
CO2_HIGH = 1200 # above this number is considered "bad" ppm
CO2_LOW = 800 # below this number is considered "good" ppm

# initialize some things
i2c = busio.I2C(board.SCL, board.SDA, frequency=50000)
sensor = adafruit_scd30.SCD30(i2c)
magtag = MagTag()
magtag.peripherals.neopixel_disable = not(lights)
if lights:
    magtag.peripherals.neopixels.brightness = 0.1
mid_x = (magtag.graphics.display.width // 2) -1

# set up the display
# co2 text, index 0
magtag.add_text(
    text_font = big_font,
    text_position = (5, (magtag.graphics.display.height // 2) + 1),
    text_anchor_point = (0.0, 0.5),
    is_data=False)
magtag.set_text(' ', auto_refresh = False)

# other data, index 1
magtag.add_text(
    text_font = little_font,
    text_anchor_point = (1.0, 0.5),
    text_position = (magtag.graphics.display.width, magtag.graphics.display.height * 0.5),
    is_data=False)
magtag.set_text('Temp: \n rH:' + ' '*15 +'\n Bat: ', index = 1, auto_refresh = False)

# give the sensor a few seconds to start up
time.sleep(3)
last_poll = -1
do_poll = True
while True:
    now = time.monotonic()
    elapsed = int(now - last_poll)
    if debug:
        print('Elapsed: ' + str(elapsed))
    if (elapsed > MAGTAG_REFRESH):
        do_poll = True
    while sensor.data_available and do_poll:
        temperature = round((sensor.temperature * 9/5) + 32, 1)
        co2 = sensor.CO2
        humidity = round(sensor.relative_humidity, 1)
        if debug:
            print('CO2: '+ str(int(co2)))
            print('Humidity: ' + str(humidity))
            print('Temperature: ' + str(temperature))
            print('Elapsed: ' + str(elapsed) + ' seconds')
        if lights:
            if co2 > CO2_HIGH:
                magtag.peripherals.neopixels.fill(red)
            elif co2 > CO2_LOW:
                magtag.peripherals.neopixels.fill(yellow)
            else:
                magtag.peripherals.neopixels.fill(green)
        co2_text = (str(int(co2)))
        other_text = 'Temp: '+ str(temperature) + '°F \n rh: ' + str(humidity) +'% \n Bat: ' + str(round(magtag.peripherals.battery, 2)) +'v'
        magtag.set_text(co2_text, index = 0)
        magtag.set_text(other_text, index = 1)
        last_poll = time.monotonic()
        do_poll = False
    # toggle lights
    if magtag.peripherals.button_a_pressed:
        print('Button A Pressed')
        lights = not(lights)
        print('lights on?' + str(lights))
        magtag.peripherals.neopixel_disable = not(lights)
        time.sleep(0.5)
    # do manual poll
    if magtag.peripherals.button_d_pressed:
        print('Button D Pressed')
        do_poll = True
        time.sleep(0.5)
