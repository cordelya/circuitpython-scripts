# MagTag Scripts #

This directory contains a collection of CircuitPython scripts meant to run on a MagTag. See top of each script for notes on required Python packages or hardware.
