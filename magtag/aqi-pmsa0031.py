import time
import terminalio
import displayio
import adafruit_imageload
import board
import busio
from adafruit_magtag.magtag import MagTag
from adafruit_pm25.i2c import PM25_I2C
from adafruit_display_text import label

BACKGROUND = 'bmps/magtag.bmp'
HEIGHT = 128
WIDTH = 296
SAMPLE_INTERVAL = 5 # minutes
H24_MAX = 288 # max num of entries to keep
H8_MAX = 96 # max num of entries to keep
H1_MAX = 12 # max num of entries to keep
pm25_24h = []
pm10_24h = []
pm25_8h = []
pm25_1h = []

reset_pin = None
i2c = busio.I2C(board.SCL, board.SDA, frequency=100000)
pm25 = PM25_I2C(i2c, reset_pin)

magtag = MagTag()
magtag.graphics.set_background(BACKGROUND)


# header group
aqi = label.Label(terminalio.FONT, text="AQI:  (24H)  (8H)  (1H)", color=0x000000, scale=2)
aqi.anchor_point = (0, 0.5)
aqi.anchored_position = (10, int(HEIGHT * 0.2))
conc_title = label.Label(terminalio.FONT, text="Concentration", color=0x000000, scale=1)
conc_title.anchor_point = (1, 0.5)
conc_title.anchored_position = ((WIDTH - 20), int(HEIGHT * 0.5))
conc = label.Label(terminalio.FONT, text="PM1.0: \nPM2.5:  \nPM 10:  ", color=0x000000, scale=1)
conc.anchor_point = (1, 0)
conc.anchored_position = ((WIDTH - 30), int(HEIGHT * 0.60))
header_group = displayio.Group()
header_group.append(aqi)
header_group.append(conc)
header_group.append(conc_title)
magtag.splash.append(header_group)


# table group
t_header = label.Label(terminalio.FONT, text="Particles per 0.1L in air", color=0x000000, scale=1)
t_header.anchor_point = (0, 0.5)
t_header.anchored_position = (25,  int(HEIGHT * 0.50))

t03 = label.Label(terminalio.FONT, text=">  0.3um\t", color=0x000000, scale=1)
t03.anchor_point = (0, 0.5)
t03.anchored_position = (10, int(HEIGHT * 0.65))

t25 = label.Label(terminalio.FONT, text=">  2.5um\t", color=0x000000, scale=1)
t25.anchor_point = (0, 0.5)
t25.anchored_position = (105, int(HEIGHT * 0.65))

t05 = label.Label(terminalio.FONT, text=">  0.5um\t", color=0x000000, scale=1)
t05.anchor_point = (0, 0.5)
t05.anchored_position = (10, int(HEIGHT * 0.75))

t50 = label.Label(terminalio.FONT, text=">  5.0um\t", color=0x000000, scale=1)
t50.anchor_point = (0, 0.5)
t50.anchored_position = (105, int(HEIGHT * 0.75))

t10 = label.Label(terminalio.FONT, text=">  1.0um\t", color=0x000000, scale=1)
t10.anchor_point = (0, 0.5)
t10.anchored_position = (10, int(HEIGHT * 0.85))

t100 = label.Label(terminalio.FONT, text="> 10.0um\t", color=0x000000, scale=1)
t100.anchor_point = (0, 0.5)
t100.anchored_position = (105, int(HEIGHT * 0.85))

table_group = displayio.Group()
table_group.append(t_header)
table_group.append(t03)
table_group.append(t25)
table_group.append(t05)
table_group.append(t50)
table_group.append(t10)
table_group.append(t100)
magtag.splash.append(table_group)
time.sleep(magtag.display.time_to_refresh + 1)
magtag.display.refresh()

while True:

    try:
        aqdata = pm25.read()
        # print(aqdata)
    except RuntimeError:
        print("Unable to read from sensor, retrying...")
        continue
    # calculate AQI 24H 8H and 1H and pop oldest readings
    # variable references:
    #H24_MAX = 288 # max num of entries to keep
    #H8_MAX = 96 # max num of entries to keep
    #H1_MAX = 12 # max num of entries to keep
    pm25_24h.append(aqdata['pm25 env'])
    pm10_24h.append(aqdata['pm10 env'])
    pm25_8h.append(aqdata['pm25 env'])
    pm25_1h.append(aqdata['pm25 env'])

    pm25_24h_high = max(pm25_24h)
    pm10_24h_high = max(pm10_24h)
    pm25_8h_high = max(pm25_8h)
    pm25_1h_high = max(pm25_1h)

    # remove overflow
    if len(pm25_24h) >= H24_MAX:
        pm25_24h.pop(0)
    if len(pm10_24h) >= H24_MAX:
        pm10_24h.pop(0)
    if len(pm25_8h) >= H8_MAX:
        pm25_8h.pop(0)
    if len(pm25_1h) >= H1_MAX:
        pm25_1h.pop(0)

    # calculate
    # PM2.5 breakpoints
    # don't bother calculating these until there are at least 18

    #truncate PM2.5 max readings to 1 decimal place
    pm25_24h_high = float(f'{pm25_24h_high:.1f}')
    pm25_8h_high = float(f'{pm25_8h_high:.1f}')
    pm25_1h_high = float(f'{pm25_8h_high:.1f}')

    #0-12
    bp_low = 0
    bp_high = 12
    i_low = 0
    i_high = 50
    if pm25_24h_high < 12:
        pm25_24h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_24h_high - bp_low) + i_low
    if pm25_8h_high < 12:
        pm25_8h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_8h_high - bp_low) + i_low
    if pm25_1h_high < 12:
        pm25_1h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_1h_high - bp_low) + i_low

    #12.1-35.4
    bp_low = 12.1
    bp_high = 35.4
    i_low = 51
    i_high = 100
    if pm25_24h_high > 12 and pm25_24h_high < 35.4:
        pm25_24h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_24h_high - bp_low) + i_low
    if pm25_8h_high > 12 and pm25_8h_high < 35.4:
        pm25_8h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_8h_high - bp_low) + i_low
    if pm25_1h_high > 12 and pm25_1h_high < 35.4:
        pm25_1h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_1h_high - bp_low) + i_low

    #35.5-55.4
    bp_low = 35.5
    bp_high = 55.4
    i_low = 101
    i_high = 150
    if pm25_24h_high > 35.4 and pm25_24h_high < 55.4:
        pm25_24h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_24h_high - bp_low) + i_low
    if pm25_8h_high > 35.4 and pm25_8h_high < 55.4:
        pm25_8h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_8h_high - bp_low) + i_low
    if pm25_1h_high > 35.4 and pm25_1h_high < 55.4:
        pm25_1h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_1h_high - bp_low) + i_low

    #55.5-150.4
    bp_low = 55.5
    bp_high = 150.4
    i_low = 151
    i_high = 500
    if pm25_24h_high > 55.4 and pm25_24h_high < 150.4:
        pm25_24h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_24h_high - bp_low) + i_low
    if pm25_8h_high > 55.4 and pm25_8h_high < 150.4:
        pm25_8h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_8h_high - bp_low) + i_low
    if pm25_1h_high > 55.4 and pm25_1h_high < 150.4:
        pm25_1h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_1h_high - bp_low) + i_low

    #150.5-250.4
    bp_low = 150.5
    bp_high = 250.4
    i_low = 201
    i_high = 300
    if pm25_24h_high > 150.4 and pm25_24h_high < 250.4:
        pm25_24h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_24h_high - bp_low) + i_low
    if pm25_8h_high > 150.4 and pm25_8h_high < 250.4:
        pm25_8h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_8h_high - bp_low) + i_low
    if pm25_1h_high > 150.4 and pm25_1h_high < 250.4:
        pm25_1h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_1h_high - bp_low) + i_low

    #250.5-500.4
    bp_low = 250.5
    bp_high = 500.4
    i_low = 301
    i_high = 500
    if pm25_24h_high > 250.4:
        pm25_24h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_24h_high - bp_low) + i_low
    if pm25_8h_high > 250.4:
        pm25_8h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_8h_high - bp_low) + i_low
    if pm25_1h_high > 250.4:
        pm25_1h_index = ((i_high - i_low) / (bp_high - bp_low)) * (pm25_1h_high - bp_low) + i_low

    if len(pm25_24h) >= (H24_MAX * 0.75):
        # if at least 18h worth of samples in the 24h list, show aqi
        text = "AQI {} 24H ".format(int(pm25_24h_index))
        print(("24H", pm25_24h_index))
    else:
        text = "AQI   24H "

    if len(pm25_8h) >= (H8_MAX * 0.75):
        text = text + "{} 8H ".format(int(pm25_8h_index))
        print(("8H", pm25_8h_index))
    else:
        text = text + "   8H "

    if len(pm25_1h) >= (H1_MAX * 0.75):
        text = text + "{} 1H".format(int(pm25_1h_index))
        print(("1H", pm25_1h_index))
    else:
        text = text + "   1H"

    aqi.text = text

        # calculate
    # PM10 breakpoints
    # maybe later.


    conc.text = (
        " PM 1.0: %d\n PM 2.5: %d\nPM 10.0: %d"
        % (aqdata["pm10 env"], aqdata["pm25 env"], aqdata["pm100 env"])
    )
    t03.text =  (">  0.3um\t{}".format(aqdata["particles 03um"]))
    t25.text =  (">  2.5um\t{}".format(aqdata["particles 25um"]))
    t05.text =  (">  0.5um\t{}".format(aqdata["particles 05um"]))
    t50.text =  (">  5.0um\t{}".format(aqdata["particles 50um"]))
    t10.text =  (">  1.0um\t{}".format(aqdata["particles 10um"]))
    t100.text = ("> 10.0um\t{}".format(aqdata["particles 100um"]))

#    print("Concentration Units (environmental)")
#    print("---------------------------------------")
#    print(
#        "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
#        % (aqdata["pm10 env"], aqdata["pm25 env"], aqdata["pm100 env"])
#    )

    print("Particles > 0.3um / 0.1L air:", aqdata["particles 03um"])
    print("Particles > 0.5um / 0.1L air:", aqdata["particles 05um"])
    print("Particles > 1.0um / 0.1L air:", aqdata["particles 10um"])
    print("Particles > 2.5um / 0.1L air:", aqdata["particles 25um"])
    print("Particles > 5.0um / 0.1L air:", aqdata["particles 50um"])
    print("Particles > 10 um / 0.1L air:", aqdata["particles 100um"])

    time.sleep(magtag.display.time_to_refresh +1)
    magtag.display.refresh()
    time.sleep(SAMPLE_INTERVAL * 60)
